/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud9.classwork.actividad14;

import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Actividad14 {

    public static void main(String[] args) {
        mostrarHoraEnZona("Europe/Madrid");
        mostrarHoraEnZona("Africa/Cairo");
        mostrarHoraEnZona("Europe/London");
        System.out.println(ZoneId.getAvailableZoneIds());
    }

    private static void mostrarHoraEnZona(String zona) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime ahoraMadrid = LocalTime.now(ZoneId.of(zona));
        System.out.printf("L'hora a %s és %s%n", zona, ahoraMadrid.format(formatter));
    }
}

