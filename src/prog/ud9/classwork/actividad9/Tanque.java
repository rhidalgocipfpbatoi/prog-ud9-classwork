/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud9.classwork.actividad9;

import prog.ud9.classwork.actividad9.exceptions.TanqueLlenoException;
import prog.ud9.classwork.actividad9.exceptions.TanqueVacioException;

public class Tanque {

    private int capacidad;
    private int carga;

    public Tanque(int capacidad) {
        this.capacidad = capacidad;
        this.carga = 0;
    }

    public void agregarCarga(int cantidad) throws TanqueLlenoException {
        if (carga+cantidad > capacidad) {
            this.carga = this.capacidad;
            throw new TanqueLlenoException();
        }

        carga += cantidad;
    }

    public void retirarCarga(int cantidad) throws TanqueVacioException {
        if (carga -cantidad < 0) {
            this.carga = 0;
            throw new TanqueVacioException("El tanque está vacío");
        }
        carga-= cantidad;
    }

    @Override
    public String toString() {
        return "Tanque{" +
                "carga=" + carga +
                '}';
    }
}


