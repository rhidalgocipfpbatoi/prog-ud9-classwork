/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud9.classwork.actividad9;

import prog.ud9.classwork.actividad9.exceptions.TanqueLlenoException;
import prog.ud9.classwork.actividad9.exceptions.TanqueVacioException;

/**
 *
 * @author batoi
 */
public class TestTanque {
    public static void main(String[] args) {
        Tanque tanque = new Tanque(100);
        Tanque tanque2 = new Tanque(20);

        try {
            tanque.agregarCarga(90);
            tanque.agregarCarga(20);
        } catch (TanqueLlenoException e) {
            System.out.println(e.getMessage());
        }

        try {
            tanque2.retirarCarga(10);
            tanque2.agregarCarga(10);
            tanque2.retirarCarga(5);
        } catch (TanqueVacioException | TanqueLlenoException e) {
            System.out.println(e.getMessage());
        }

        System.out.println(tanque);
        System.out.println(tanque2);
    }
}

