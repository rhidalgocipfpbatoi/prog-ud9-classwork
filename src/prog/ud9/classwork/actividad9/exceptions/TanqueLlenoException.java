/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud9.classwork.actividad9.exceptions;

/**
 *
 * @author batoi
 */
public class TanqueLlenoException extends Exception{
    public TanqueLlenoException() {
        super("El tanque está lleno");
    }
}

