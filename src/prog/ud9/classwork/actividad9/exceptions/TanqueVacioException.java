/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud9.classwork.actividad9.exceptions;

/**
 *
 * @author batoi
 */
public class TanqueVacioException extends Exception {
    public TanqueVacioException(String message) {
        super(message);
    }
}

