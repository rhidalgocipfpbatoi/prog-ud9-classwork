/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud9.classwork.actividad7;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Actividad7 {

    private static final Scanner scanner = new Scanner(System.in);
    private static final int VALOR_MIN = 10;
    private static final int VALOR_MAX = 50;

    public static void main(String[] args) {
        do {
            try {
                String mensaje = String.format("Introduce un valor [%d-%d]",
                        VALOR_MIN, VALOR_MAX);
                int numero = obtenerEntero(mensaje, VALOR_MIN, VALOR_MAX);
                System.out.println(numero);
                return;
            } catch (InputMismatchException ex) {
                System.out.println(ex.getMessage());
            }
        }while(true);
    }

    private static int obtenerEntero(String mensaje) {
        do {
            System.out.println(mensaje);
            if (scanner.hasNextInt()) {
                return scanner.nextInt();
            }
            scanner.next();
            System.out.println("Error. Debes introducir un número");
        } while(true);
    }

    public static int obtenerEntero(String mensaje, int min, int max) {

        int numero = obtenerEntero(mensaje);
        if (numero < min || numero > max) {
            throw new InputMismatchException("Error. Número fuera de rango");
        }

        return numero;
    }
}
