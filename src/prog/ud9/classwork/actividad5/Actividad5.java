/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud9.classwork.actividad5;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Actividad5 {

    public static void main(String[] args) {

        //case valido
        int[] array5 = new int[5];
        readNumbers(array5, 5);

        //case invalido
        readNumbers(array5, 6);

        //case invalido
        int[] array2 = null;
        readNumbers(array2, 10);

    }

    public static void readNumbers(int[] numbers, int size) {
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < size; i++) {
            try {
                System.out.println("Introduce un número");
                numbers[i] = input.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Debe introducir un número");
                input.nextLine();
                i--;
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("El array ya está completo");
                return;
            } catch (NullPointerException e) {
                System.out.println("El array no esta inicializado");
                return;
            }
        }

    }
}
