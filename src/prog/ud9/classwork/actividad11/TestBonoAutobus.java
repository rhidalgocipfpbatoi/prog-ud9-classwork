/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud9.classwork.actividad11;

import prog.ud9.classwork.actividad11.exception.SaldoAgotadoException;

/**
 * Actividad12. Crea una clase BonoAutobus que represente un bono de 10 viajes para subir a un autobus metropolitano.
 * Esta clase dispondrá de una propiedad saldo que indique el número de viajes disponibles, y que serán establecidos
 * en el momento de su inicialización. Dispondrá de una metodo fichar el cual en caso de que no disponga de saldo, lanzará una excepción de tipo SaldoAgotadoException.
 * Crea una clase TestBonoAutobus que intente subir + de 10 veces al autobus.
 */
public class TestBonoAutobus {

    public static void main(String[] args) {

        BonoAutobus bono = new BonoAutobus();
        try {
            for (int i = 0; i <= 20; i++) {
                System.out.println("Voy a subir al autobus vez " + i);
                bono.fichar();
            }
        } catch (SaldoAgotadoException e) {
            System.out.println(e.getMessage());
        }
    }

}

