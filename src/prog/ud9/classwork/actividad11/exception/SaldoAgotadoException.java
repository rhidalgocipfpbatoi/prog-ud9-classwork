/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud9.classwork.actividad11.exception;

public class SaldoAgotadoException extends Exception {

    public SaldoAgotadoException(){
        super("No dispone de saldo para subir al autobus");
    }

}
