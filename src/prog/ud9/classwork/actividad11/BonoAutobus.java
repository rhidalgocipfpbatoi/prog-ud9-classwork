/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud9.classwork.actividad11;

import prog.ud9.classwork.actividad11.exception.SaldoAgotadoException;


public class BonoAutobus {

    private final int DEFAULT_SALDO = 10;

    private int saldo;

    public BonoAutobus(){
        saldo = DEFAULT_SALDO;
    }

    public void fichar() throws SaldoAgotadoException {
        if (saldo == 0) {
            throw new SaldoAgotadoException();
        }
        saldo--;
    }

}


