/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud9.classwork.actividad12;

import java.time.LocalDateTime;
import prog.ud9.classwork.actividad7.Actividad7;

/**
 *
 * @author batoi
 */
public class Actividad12 {
    private static final int OPCION_DIA = 1;
    private static final int OPCION_MES = 2;
    private static final int OPCION_ANYO = 3;
    public static void main(String[] args) {
        System.out.println("La data i l’hora actual del sistema és:");
        LocalDateTime ahora = LocalDateTime.now();
        System.out.println(ahora);
        String mensaje = String.format("%s%n%d) %s%n%d) %s%n%d) %s",
                "Què vols visualitzar?",OPCION_DIA, "Dia", OPCION_MES, "Mes", OPCION_ANYO, "Any");

        int opcion = Actividad7.obtenerEntero(mensaje, OPCION_DIA, OPCION_ANYO);

        switch (opcion) {
            case OPCION_DIA -> System.out.printf("Dia = %02d", ahora.getDayOfMonth());
            case OPCION_MES -> System.out.printf("Mes = %s", ahora.getMonth().getValue());
            case OPCION_ANYO -> System.out.printf("Any = %4d", ahora.getYear());
        }
    }

}
