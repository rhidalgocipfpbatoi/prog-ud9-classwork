package prog.ud9.classwork.actividad10.types;

public class Lion extends Animal{

    public Lion(Food food, Size size, String location) {
        super(food, size, location, "Leon");
    }

    @Override
    public void makeNoyse() {
        System.out.println("ARGHHHHHHHH!!!!!!!!!");
    }
}
