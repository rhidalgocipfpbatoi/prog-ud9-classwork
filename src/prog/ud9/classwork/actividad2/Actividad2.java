/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud9.classwork.actividad2;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Actividad2 {
    
    public static void main(String[] args) {
       
        
        int contador = 0;
        int maximo = Integer.MIN_VALUE;
        
        do {
            try {
                int numero = obtenerEntero();
                if (numero > maximo) {
                    maximo = numero;
                }

                contador++;
            } catch (InputMismatchException ex) {
                System.out.println("Hay que introducir un número");
            }

        } while(contador < 6);
    }
    
    private static int obtenerEntero() {
        
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
    
}
