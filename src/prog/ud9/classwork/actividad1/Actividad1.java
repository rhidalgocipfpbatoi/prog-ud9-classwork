/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud9.classwork.actividad1;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Actividad1 {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int contador = 0;
        int maximo = Integer.MIN_VALUE;
        
        do {
            try {
                int numero = scanner.nextInt();
                if (numero > maximo) {
                    maximo = numero;
                }

                contador++;
            } catch (InputMismatchException ex) {
                System.out.println("Hay que introducir un número");
            }

        } while(contador < 6);
    }
    
}
