/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud9.classwork.actividad8;

/**
 *
 * @author batoi
 */
public class WaitApp {
    
    public static void main(String[] args) {
        try {
            waitSeconds(5);
            System.out.println("Fin");
        } catch(InterruptedException ex) {
            System.out.println(ex.getMessage());
        }
    }
    private static void waitSeconds(int seconds) throws InterruptedException{
        Thread.sleep(seconds * 1000);
    }
}
